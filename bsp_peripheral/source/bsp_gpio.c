/* Includes ------------------------------------------------------------------*/
#include "bsp_gpio.h"

bsp_gpio_t *bsp_gpio;

int32_t bsp_gpio_init(void *handle, bsp_gpio_mode_t mode, uint32_t state)
{
  int32_t res = 0;

  bsp_gpio = (bsp_gpio_t *)handle;

  res = bsp_gpio->gpio_init(handle, mode, state);

  return res;
}


int32_t bsp_gpio_deinit(void *handle)
{
  int32_t res = 0;

  bsp_gpio = (bsp_gpio_t *)handle;

  res = bsp_gpio->gpio_deinit(handle);

  return res;
}


int32_t bsp_gpio_write(void *handle, uint32_t state)
{
  int32_t res = 0;

  bsp_gpio = (bsp_gpio_t *)handle;

  res = bsp_gpio->gpio_write(handle, state);

  return res;
}


int32_t bsp_gpio_read(void *handle, uint32_t *state)
{
  int32_t res = 0;

  bsp_gpio = (bsp_gpio_t *)handle;

  res = bsp_gpio->gpio_read(handle, state);

  return res;
}




