/* Includes ------------------------------------------------------------------*/
#include "bsp_spi.h"

bsp_spi_t *bsp_spi;

int32_t bsp_spi_init(void *handle, bsp_spi_mode_t mode, uint32_t max_clk_freq)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_init(handle, mode, max_clk_freq);

  return res;
}


int32_t bsp_spi_deinit(void *handle)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_deinit(handle);

  return res;
}


int32_t bsp_spi_set_mode(void *handle, bsp_spi_mode_t mode)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_set_mode(handle, mode);

  return res;
}


int32_t bsp_spi_set_max_clk_freq(void *handle, uint32_t max_clk_freq)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_set_max_clk_freq(handle, max_clk_freq);

  return res;
}


int32_t bsp_spi_write(void *handle, void *txdata, uint32_t length)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_write(handle, txdata, length);

  return res;
}


int32_t bsp_spi_read(void *handle, void *rxdata, uint32_t length)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_read(handle, rxdata, length);

  return res;
}


int32_t bsp_spi_transfer(void *handle, void *txdata, void *rxdata, uint32_t length)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_transfer(handle, txdata, rxdata, length);

  return res;
}


int32_t bsp_spi_write_dma(void *handle, void *txdata, uint32_t length)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_write_dma(handle, txdata, length);

  return res;
}


int32_t bsp_spi_read_dma(void *handle, void *rxdata, uint32_t length)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_read_dma(handle, rxdata, length);

  return res;
}


int32_t bsp_spi_transfer_dma(void *handle, void *txdata, void *rxdata, uint32_t length)
{
  int32_t res = 0;

  bsp_spi = (bsp_spi_t *)handle;

  res = bsp_spi->spi_transfer_dma(handle, txdata, rxdata, length);

  return res;
}




