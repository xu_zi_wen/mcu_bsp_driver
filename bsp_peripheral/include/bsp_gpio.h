/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_GPIO_H__
#define __BSP_GPIO_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>


typedef enum _bsp_gpio_mode_t
{
  BSP_GPIO_MODE_ANALOG              = 0,
  BSP_GPIO_MODE_INPUT               = 1,
  BSP_GPIO_MODE_OUTPUT_PP           = 2,
  BSP_GPIO_MODE_OUTPUT_OD           = 3,
  BSP_GPIO_MODE_AF_PP               = 4,
  BSP_GPIO_MODE_AF_OD               = 5,
  BSP_GPIO_MODE_IT_RISING           = 6,
  BSP_GPIO_MODE_IT_FALLING          = 7,
  BSP_GPIO_MODE_IT_RISING_FALLING   = 8,
  BSP_GPIO_MODE_EVT_RISING          = 9,
  BSP_GPIO_MODE_EVT_FALLING         = 10,
  BSP_GPIO_MODE_EVT_RISING_FALLING  = 11,
} bsp_gpio_mode_t;


typedef int32_t (*gpio_init_func)(void *, bsp_gpio_mode_t, uint32_t);
typedef int32_t (*gpio_deinit_func)(void *);
typedef int32_t (*gpio_write_func)(void *, uint32_t);
typedef int32_t (*gpio_read_func)(void *, uint32_t *);


typedef struct _bsp_gpio_t
{
  void *gpio_handle;
  gpio_init_func gpio_init;
  gpio_deinit_func gpio_deinit;
  gpio_write_func gpio_write;
  gpio_read_func gpio_read;
} bsp_gpio_t;


int32_t bsp_gpio_init(void *handle, bsp_gpio_mode_t mode, uint32_t state);
int32_t bsp_gpio_deinit(void *handle);
int32_t bsp_gpio_write(void *handle, uint32_t state);
int32_t bsp_gpio_read(void *handle, uint32_t *state);


#ifdef __cplusplus
}
#endif
#endif


