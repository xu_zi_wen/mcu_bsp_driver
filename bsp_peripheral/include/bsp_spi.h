/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_SPI_H__
#define __BSP_SPI_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>


typedef enum _bsp_spi_mode_t
{
  BSP_SPI_MODE_0 = 0,  // CPOL = 0, CPHA = 0
  BSP_SPI_MODE_1 = 1,  // CPOL = 0, CPHA = 1
  BSP_SPI_MODE_2 = 2,  // CPOL = 1, CPHA = 0
  BSP_SPI_MODE_3 = 3,  // CPOL = 1, CPHA = 1
} bsp_spi_mode_t;


typedef int32_t (*spi_init_func)(void *, bsp_spi_mode_t, uint32_t);
typedef int32_t (*spi_deinit_func)(void *);
typedef int32_t (*spi_set_mode_func)(void *, bsp_spi_mode_t);
typedef int32_t (*spi_set_clk_freq_func)(void *, uint32_t);
typedef int32_t (*spi_write_func)(void *, void *, uint32_t);
typedef int32_t (*spi_read_func)(void *, void *, uint32_t);
typedef int32_t (*spi_transfer_func)(void *, void *, void *, uint32_t);


typedef struct _bsp_spi_t
{
  void *spi_handle;
  spi_init_func spi_init;
  spi_deinit_func spi_deinit;
  spi_set_mode_func spi_set_mode;
  spi_set_clk_freq_func spi_set_max_clk_freq;
  spi_write_func spi_write;
  spi_read_func spi_read;
  spi_transfer_func spi_transfer;
  spi_write_func spi_write_dma;
  spi_read_func spi_read_dma;
  spi_transfer_func spi_transfer_dma;
} bsp_spi_t;


int32_t bsp_spi_init(void *handle, bsp_spi_mode_t mode, uint32_t max_clk_freq);
int32_t bsp_spi_deinit(void *handle);
int32_t bsp_spi_set_mode(void *handle, bsp_spi_mode_t mode);
int32_t bsp_spi_set_max_clk_freq(void *handle, uint32_t max_clk_freq);
int32_t bsp_spi_write(void *handle, void *txdata, uint32_t length);
int32_t bsp_spi_read(void *handle, void *rxdata, uint32_t length);
int32_t bsp_spi_transfer(void *handle, void *txdata, void *rxdata, uint32_t length);
int32_t bsp_spi_write_dma(void *handle, void *txdata, uint32_t length);
int32_t bsp_spi_read_dma(void *handle, void *rxdata, uint32_t length);
int32_t bsp_spi_transfer_dma(void *handle, void *txdata, void *rxdata, uint32_t length);


#ifdef __cplusplus
}
#endif
#endif


