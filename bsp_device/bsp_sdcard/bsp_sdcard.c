#include "bsp_sdcard.h"


BSP_SDCard_t *bsp_sdcard;

/**
  * @brief  SD card Init.
  * @param  Instance  SD Instance
  * @retval BSP status
  */
int32_t BSP_SD_Card_Init(void *Handle)
{
  bsp_sdcard = Handle;
  
  bsp_sdcard->sd_init(Handle);
  
  return 0;
}


/**
  * @brief  SD card DeInit.
  * @param  Instance  SD Instance
  * @retval BSP status
  */
int32_t BSP_SD_Card_DeInit(void *Handle)
{
  bsp_sdcard = Handle;
  
  bsp_sdcard->sd_deinit(Handle);
  
  return 0;
}


/**
  * @brief  Gets the current SD card data status.
  * @param  Instance  SD Instance
  * @retval Data transfer state.
  *          This value can be one of the following values:
  *            @arg  SD_TRANSFER_OK: No data transfer is acting
  *            @arg  SD_TRANSFER_BUSY: Data transfer is acting
  */
int32_t BSP_SD_GetCardState(void *Handle)
{
  bsp_sdcard = Handle;
  
  bsp_sdcard->sd_get_state(Handle);
  
  return 0;
}


/**
  * @brief  Get SD information about specific SD card.
  * @param  Instance  SD Instance
  * @param  CardInfo  Pointer to HAL_SD_CardInfoTypedef structure
  * @retval BSP status
  */
int32_t BSP_SD_GetCardInfo(void *Handle, BSP_SD_CardInfo_t *CardInfo)
{
  bsp_sdcard = Handle;
  
  bsp_sdcard->sd_get_info(Handle, CardInfo);
  
  return 0;
}


/**
  * @brief  Reads block(s) from a specified address in an SD card, in polling mode.
  * @param  Instance   SD Instance
  * @param  pData      Pointer to the buffer that will contain the data to transmit
  * @param  BlockIdx   Block index from where data is to be read
  * @param  BlocksNbr  Number of SD blocks to read
  * @retval BSP status
  */
int32_t BSP_SD_ReadBlocks(void *Handle, uint8_t *pData, uint32_t BlockIndex, uint32_t BlocksCount)
{
  bsp_sdcard = Handle;
  
  bsp_sdcard->sd_read(Handle, pData, BlockIndex, BlocksCount);
  
  return 0;
}


/**
  * @brief  Writes block(s) to a specified address in an SD card, in polling mode.
  * @param  Instance   SD Instance
  * @param  pData      Pointer to the buffer that will contain the data to transmit
  * @param  BlockIdx   Block index from where data is to be written
  * @param  BlocksNbr  Number of SD blocks to write
  * @retval BSP status
  */
int32_t BSP_SD_WriteBlocks(void *Handle, uint8_t *pData, uint32_t BlockIndex, uint32_t BlocksCount)
{
  bsp_sdcard = Handle;
  
  bsp_sdcard->sd_write(Handle, pData, BlockIndex, BlocksCount);
  
  return 0;
}


/**
  * @brief  Reads block(s) from a specified address in an SD card, in DMA mode.
  * @param  Instance   SD Instance
  * @param  pData      Pointer to the buffer that will contain the data to transmit
  * @param  BlockIdx   Block index from where data is to be read
  * @param  BlocksNbr  Number of SD blocks to read
  * @retval BSP status
  */
int32_t BSP_SD_ReadBlocks_DMA(void *Handle, uint8_t *pData, uint32_t BlockIndex, uint32_t BlocksCount)
{
  bsp_sdcard = Handle;
  
  bsp_sdcard->sd_read_dma(Handle, pData, BlockIndex, BlocksCount);
  
  return 0;
}


/**
  * @brief  Writes block(s) to a specified address in an SD card, in DMA mode.
  * @param  Instance   SD Instance
  * @param  pData      Pointer to the buffer that will contain the data to transmit
  * @param  BlockIdx   Block index from where data is to be written
  * @param  BlocksNbr  Number of SD blocks to write
  * @retval BSP status
  */
int32_t BSP_SD_WriteBlocks_DMA(void *Handle, uint8_t *pData, uint32_t BlockIndex, uint32_t BlocksCount)
{
  bsp_sdcard = Handle;
  
  bsp_sdcard->sd_write_dma(Handle, pData, BlockIndex, BlocksCount);
  
  return 0;
}



