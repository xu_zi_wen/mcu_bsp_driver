#ifndef __BSP_SDCARD_H__
#define __BSP_SDCARD_H__	 

#include <stdint.h>

/** @defgroup Exported 
  * @{
  */
#define SD_INSTANCES_NBR         1UL

#ifndef SD_WRITE_TIMEOUT
  #define SD_WRITE_TIMEOUT       100U
#endif

#ifndef SD_READ_TIMEOUT
  #define SD_READ_TIMEOUT        100U
#endif

/**
  * @brief  SD transfer state definition
  */
#define   SD_TRANSFER_OK         0U
#define   SD_TRANSFER_BUSY       1U

/**
  * @brief SD-detect signal
  */
#define SD_PRESENT               1UL
#define SD_NOT_PRESENT           0UL


typedef struct __BSP_SD_CardInfo_t
{
  uint8_t Type;
  uint32_t Capacity;
  uint32_t BlocksCount;
  uint32_t BlockSize;
} BSP_SD_CardInfo_t;

typedef int32_t (*sd_init_func)(void *);
typedef int32_t (*sd_deinit_func)(void *);
typedef int32_t (*sd_get_state_func)(void *);
typedef int32_t (*sd_get_info_func)(void *, BSP_SD_CardInfo_t *);
typedef int32_t (*sd_write_func)(void *, uint8_t *, uint32_t, uint32_t);
typedef int32_t (*sd_read_func)(void *, uint8_t *, uint32_t, uint32_t);

typedef struct __BSP_SDCard_t
{
  uint32_t sd_handle;
  sd_init_func sd_init;
  sd_deinit_func sd_deinit;
  sd_get_state_func sd_get_state;
  sd_get_info_func sd_get_info;
  sd_write_func sd_write;
  sd_read_func sd_read;
  sd_write_func sd_write_dma;
  sd_read_func sd_read_dma;
} BSP_SDCard_t;

/* BSP SD Card API */
int32_t BSP_SD_Card_Init(void *Handle);
int32_t BSP_SD_Card_DeInit(void *Handle);
int32_t BSP_SD_GetCardState(void *Handle);
int32_t BSP_SD_GetCardInfo(void *Handle, BSP_SD_CardInfo_t *CardInfo);
int32_t BSP_SD_ReadBlocks(void *Handle, uint8_t *pData, uint32_t BlockIndex, uint32_t BlocksCount);
int32_t BSP_SD_WriteBlocks(void *Handle, uint8_t *pData, uint32_t BlockIndex, uint32_t BlocksCount);
int32_t BSP_SD_ReadBlocks_DMA(void *Handle, uint8_t *pData, uint32_t BlockIndex, uint32_t BlocksCount);
int32_t BSP_SD_WriteBlocks_DMA(void *Handle, uint8_t *pData, uint32_t BlockIndex, uint32_t BlocksCount);


#endif


